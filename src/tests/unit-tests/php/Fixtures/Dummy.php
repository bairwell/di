<?php
namespace Bairwell\DI\Tests\Fixtures;

class Dummy
{

    public function demo($var)
    {
        return ($var + 1);
    }

    private $data;

    public function setData($data) {
        $this->data=$data;
    }

    public function getData() {
        return $this->data;
    }
}