<?php
namespace Bairwell\DI\Tests\Fixtures;

class ValueObject implements \Bairwell\DI\IValueObject
{

    private $data;

    public function setData($data) {
        $this->data=$data;
    }

    public function getData() {
        return $this->data;
    }

    public function demo($var)
    {
        return ($var + 1);
    }
}