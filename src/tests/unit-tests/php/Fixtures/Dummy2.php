<?php
namespace Bairwell\DI\Tests\Fixtures;

class Dummy2
{

    public function demo($var)
    {
        return ($var + 1);
    }

    public function setData($data) {
        $this->data=$data;
    }

    public function getData() {
        return $this->data;
    }
}