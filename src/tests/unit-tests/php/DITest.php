<?php
/**
 * Tests the Dependency Injector.
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage DI
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\DI\Tests;
/**
 * Tests the DI class
 */
class DI extends \PHPUnit_Framework_TestCase
{
    /**
     * Tear down/reset the factory each time
     *
     * @return void
     */
    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    public function testConstructor()
    {
        $emess = NULL;
        try {
            $new = new \Bairwell\DI();
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals($emess, 'The Dependency Injector must be called as a singleton');
    }

    /**
     * We should only be able to fetch mocks when mocks is on. Throw an
     * error if this isn't the case.
     *
     * @return void
     */
    public function testGetLibraryExceptionsMocksOnButNotMock()
    {
        \Bairwell\DI::reset();
        $dummyLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\Dummy', $dummyLibrary);
        \Bairwell\DI::mocksOnly(TRUE);
        $emess = NULL;
        try {
            $dummyLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Requested \Bairwell\DI\Tests\Fixtures\Dummy, but that is not a Closure or '
                . 'PHPUnit_Framework_MockObject_MockObject in "mocksOnly" mode'
        );
    }

    /**
     * Calls to GetLibrary need to include the back slash
     *
     * @return void
     */
    public function testGetLibraryExceptionsNoBackSlash()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            $dummyLibrary = \Bairwell\DI::getLibrary('Bairwell\DI\Tests\Fixtures\Dummy');
        } catch (\Bairwell\DI\BackSlashException $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Calls need to include the full namespace and classname with leading slash: '
                . 'Attempted to call: Bairwell\DI\Tests\Fixtures\Dummy'
        );
    }

    /**
     * We should throw an error if mocks are on and if we try and fetch something that isn't a mock
     *
     * @return void
     */
    public function testGetLibraryExceptionsMocksOnButNotCreated()
    {
        \Bairwell\DI::reset();
        $dummyLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\Dummy', $dummyLibrary);
        \Bairwell\DI::mocksOnly(TRUE);
        $emess = NULL;
        try {
            $subdivisionLibrary = \Bairwell\DI::getLibrary('\ABC');
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Requested \ABC, but that is not a defined Closure or '
                . 'PHPUnit_Framework_MockObject_MockObject in "mocksOnly" mode'
        );
    }

    /**
     * Get a library function
     *
     * @return void
     */
    public function testGetLibrary()
    {
        \Bairwell\DI::reset();
        $dummyLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\Dummy', $dummyLibrary);
        $dummyLibrary->setData('test');
        $this->assertEquals('test',$dummyLibrary->getData());
        $secondLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy2');
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\Dummy2', $secondLibrary);
        $secondLibrary->setData('hello');
        $this->assertEquals('hello',$secondLibrary->getData());
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\Dummy', $dummyLibrary);
        /**
         * Is the first one the same?
         */
        $this->assertEquals('test',$dummyLibrary->getData());
        /**
         * If we get the first library again, is the contents the same?
         */
        $thirdLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\Dummy', $thirdLibrary);
        $this->assertEquals('test',$dummyLibrary->getData());
        $this->assertEquals('hello',$secondLibrary->getData());
        $this->assertEquals('test',$thirdLibrary->getData());
    }

    /**
     * Fail to get a library function
     *
     * @return void
     */
    public function testGetLibraryFails()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            $dummyLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\DoesNotExist');
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals('Dependency injector unable to load class \Bairwell\DI\Tests\Fixtures\DoesNotExist', $emess);
    }

    /**
     * Test adding mocks
     *
     * @return void
     */
    public function testGetLibraryTestAddingMocks()
    {
        \Bairwell\DI::reset();

        $mock = $this->getMock('\Bairwell\DI\Tests\Fixtures\Dummy');
        $mock->expects($this->any())->method('demo');
        \Bairwell\DI::addLibrary('\Bairwell\DI\Tests\Fixtures\Dummy', $mock);
        $mocked = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $this->assertInstanceOf('\PHPUnit_Framework_MockObject_MockObject', $mocked);
        $emess = NULL;
        try {
            \Bairwell\DI::addLibrary('\Bairwell\DI\Tests\Fixtures\Dummy', $mock);
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Class \Bairwell\DI\Tests\Fixtures\Dummy already exists: cannot add to factory'
        );
    }

    /**
     * Calls to AddLibrary need to include the back slash
     *
     * @return void
     */
    public function testAddLibraryExceptionsNoBackSlash()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            $mocked = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
            $dummyLibrary = \Bairwell\DI::addLibrary('Bairwell\DI\Tests\Fixtures\Dummy', $mocked);
        } catch (\Bairwell\DI\BackSlashException $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Calls need to include the full namespace and classname with leading slash: '
                . 'Attempted to call: Bairwell\DI\Tests\Fixtures\Dummy'
        );
    }

    /**
     * Test loading mocks from the factory
     *
     * @return void
     */
    public function testGetLibraryLoadingMocks()
    {
        \Bairwell\DI::reset();

        $mock = $this->getMock('\Bairwell\DI\Tests\Fixtures\Dummy');
        $mock->expects($this->any())->method('all');
        \Bairwell\DI::addLibrary('\Bairwell\DI\Tests\Fixtures\Dummy', $mock);
        $mocked = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $this->assertInstanceOf('\PHPUnit_Framework_MockObject_MockObject', $mocked);
        \Bairwell\DI::mocksOnly(TRUE);
        $mocked = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $this->assertInstanceOf('\PHPUnit_Framework_MockObject_MockObject', $mocked);
    }

    /**
     * Test deleting mocks from the factory
     *
     * @return void
     */
    public function testGetLibraryTestDeletingMocks()
    {
        \Bairwell\DI::reset();
        $mock = $this->getMock('\Bairwell\DI\Tests\Fixtures\Dummy');
        $mock->expects($this->any())->method('all');
        \Bairwell\DI::addLibrary('\Bairwell\DI\Tests\Fixtures\Dummy', $mock);
        \Bairwell\DI::deleteLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        $emess = NULL;
        try {
            \Bairwell\DI::deleteLibrary('\Bairwell\DI\Tests\Fixtures\Dummy');
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Class \Bairwell\DI\Tests\Fixtures\Dummy does not exist: the factory cannot delete it'
        );
    }

    /**
     * Calls to DeleteLibrary need to include the back slash
     *
     * @return void
     */
    public function testDeleteLibraryExceptionsNoBackSlash()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            \Bairwell\DI::deleteLibrary('Bairwell\DI\Tests\Fixtures\Dummy');
        } catch (\Bairwell\DI\BackSlashException $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Calls need to include the full namespace and classname with leading slash: '
                . 'Attempted to call: Bairwell\DI\Tests\Fixtures\Dummy'
        );
    }

    /**
     * Calls to GetLibrary need to include the back slash
     *
     * @return void
     */
    public function testGetValueObjectExceptionsNoBackSlash()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            $dummyLibrary = \Bairwell\DI::getValueObject('Bairwell\DI\Tests\Fixtures\Dummy');
        } catch (\Bairwell\DI\BackSlashException $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals(
            $emess,
            'Calls need to include the full namespace and classname with leading slash: '
                . 'Attempted to call: Bairwell\DI\Tests\Fixtures\Dummy'
        );
    }

    /**
     * Fail to get a library function
     *
     * @return void
     */
    public function testGetValueObjectFails()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            $dummyLibrary = \Bairwell\DI::getValueObject('\Bairwell\DI\Tests\Fixtures\DoesNotExist');
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals('Dependency injector unable to load class \Bairwell\DI\Tests\Fixtures\DoesNotExist', $emess);
    }

    /**
     * Fail to get something that is a value object
     *
     * @return void
     */
    public function testGetLibraryIsAValueObject()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            $dummyLibrary = \Bairwell\DI::getLibrary('\Bairwell\DI\Tests\Fixtures\ValueObject');
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals('\Bairwell\DI\Tests\Fixtures\ValueObject is a value object (supports \Bairwell\DI\IValueObject interface)', $emess);
    }

    /**
     * Fail to get something that is not a value object
     *
     * @return void
     */
    public function testGetValueObjectNotValueObject()
    {
        \Bairwell\DI::reset();
        $emess = NULL;
        try {
            $dummyLibrary = \Bairwell\DI::getValueObject('\Bairwell\DI\Tests\Fixtures\Dummy');
        } catch (\Bairwell\DI\Exception $e) {
            $emess = $e->getMessage();
        } catch (\Exception $e) {
            $emess = 'Did not raise correct Exception type';
        }
        $this->assertEquals('\Bairwell\DI\Tests\Fixtures\Dummy does not support the \Bairwell\DI\IValueObject interface', $emess);
    }

    /**
     * Get a value object
     *
     * @return void
     */
    public function testGetValueObject()
    {
        \Bairwell\DI::reset();
        $vo = \Bairwell\DI::getValueObject('\Bairwell\DI\Tests\Fixtures\ValueObject');
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\ValueObject', $vo);
        $vo->setData('test');
        $this->assertEquals('test',$vo->getData());
        $vo2 = \Bairwell\DI::getValueObject('\Bairwell\DI\Tests\Fixtures\ValueObject');
        $this->assertInstanceOf('\Bairwell\DI\Tests\Fixtures\ValueObject', $vo2);
        $vo2->setData('hello');
        /**
         * Does the original still hold the same content
         */
        $this->assertEquals('test',$vo->getData());
        /**
         * And does the new one hold different content?
         */
        $this->assertEquals('hello',$vo2->getData());
    }

}