<?php
/**
 * This is the exception called if a back slash is not sent as the first character of the
 * call to a library function
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage DI
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\DI;

/**
 * Exception raised if the class name does not start with a slash.
 */
class BackSlashException extends Exception
{
    /**
     * Redefine the exception so message isn't optional
     * @param string $message What file was trying to be loaded
     * @param int $code Any code we are sending back
     * @param \Exception|null $previous Any previous exceptions
     */
    public function __construct($message, $code = 0, \Exception $previous = NULL)
    {
        $messagePrefix = 'Calls need to include the full namespace and classname with leading slash: Attempted to call:';
        // make sure everything is assigned properly
        parent::__construct($messagePrefix . ' ' . $message, $code, $previous);
    }
}