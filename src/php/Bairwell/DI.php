<?php
/**
 * This is the main Dependency Injection class used by Bairwell items.
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage DI
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell;

/**
 * A basic DI class to enable Unittesting.
 */
class DI
{

    /**
     * This holds the created instances of objects
     *
     * @var array
     */
    private static $_objects;

    /**
     * Should we be enforcing mock objects only?
     *
     * @var bool
     */
    private static $_mocksOnly = FALSE;

    /**
     * Cannot call constructor from outside the class.
     * Helps enforce singletons
     *
     */
    public function __construct()
    {
        throw new \Bairwell\DI\Exception('The Dependency Injector must be called as a singleton');
    }

    /**
     * Returns an appropriate class/object
     *
     * If mocksOnly is enabled, then only Mocks or Closures will be returned (ideal for unit testing)
     *
     * @static
     * @param string $name The name of the class we are after
     * @return object The created object
     */
    public static function getLibrary($name)
    {
        if (mb_substr($name, 0, 1) !== '\\') {
            throw new \Bairwell\DI\BackSlashException($name);
        }
        if (static::$_mocksOnly === TRUE) {
            if (isset(static::$_objects[$name]) === TRUE) {
                if (static::$_objects[$name] instanceof \Closure
                    || static::$_objects[$name] instanceof \PHPUnit_Framework_MockObject_MockObject
                ) {
                    return static::$_objects[$name];
                } else {
                    $message = 'Requested ' . $name . ', but that is not a Closure or '
                        . 'PHPUnit_Framework_MockObject_MockObject in "mocksOnly" mode';
                    throw new \Bairwell\DI\Exception($message);
                }
            } else {
                $message = 'Requested ' . $name . ', but that is not a defined Closure or '
                    . 'PHPUnit_Framework_MockObject_MockObject in "mocksOnly" mode';
                throw new \Bairwell\DI\Exception($message);
            }
        }
        if (isset(static::$_objects[$name]) === TRUE) {
            return static::$_objects[$name];
        }
        if (class_exists($name, TRUE) === FALSE) {
            throw new \Bairwell\DI\Exception('Dependency injector unable to load class ' . $name);
        }
        static::$_objects[$name] = new $name;
        if ((static::$_objects[$name] instanceof \Bairwell\DI\IValueObject) === TRUE) {
            unset(static::$_objects[$name]);
            throw new \Bairwell\DI\Exception($name . ' is a value object (supports \Bairwell\DI\IValueObject interface)');
        }
        return static::$_objects[$name];

    }

    /**
     * Return a new value object store
     * @static
     * @param string $name The name of the class we are after
     * @return object The created object
     * @throws DI\BackSlashException|DI\Exception
     */
    public static function getValueObject($name)
    {
        if (mb_substr($name, 0, 1) !== '\\') {
            throw new \Bairwell\DI\BackSlashException($name);
        }
        if (class_exists($name, TRUE) === FALSE) {
            throw new \Bairwell\DI\Exception('Dependency injector unable to load class ' . $name);
        }
        $newClass = new $name;
        if (($newClass instanceof \Bairwell\DI\IValueObject) === FALSE) {
            throw new \Bairwell\DI\Exception($name . ' does not support the \Bairwell\DI\IValueObject interface');
        }
        return $newClass;
    }

    /**
     * Should we be enforcing mocks only?
     *
     * @static
     * @param bool $setting TRUE to enforce Mocks only, FALSE to allow all types of objects
     * @return void
     */
    public static function mocksOnly($setting)
    {
        static::$_mocksOnly = $setting;
    }

    /**
     * Adds a library class to our cache - ideal for unit testing and mocks
     *
     * @static
     * @throws \Exception
     * @param string $name The name of the class we are adding
     * @param object $mock The mock object or similar we are adding to the cache
     * @return void
     */
    public static function addLibrary($name, $mock)
    {
        if (mb_substr($name, 0, 1) !== '\\') {
            throw new \Bairwell\DI\BackSlashException($name);
        }
        if (isset(static::$_objects[$name]) === TRUE) {
            throw new \Bairwell\DI\Exception('Class ' . $name . ' already exists: cannot add to factory');
        }
        static::$_objects[$name] = $mock;
    }

    /**
     * Forget/delete an object we should know about (ideal for removing mocks)
     *
     * @static
     * @throws \Exception
     * @param string $name The class to forget about
     * @return void
     */
    public static function deleteLibrary($name)
    {
        if (mb_substr($name, 0, 1) !== '\\') {
            throw new \Bairwell\DI\BackSlashException($name);
        }
        if (isset(static::$_objects[$name]) === FALSE) {
            throw new \Bairwell\DI\Exception('Class ' . $name . ' does not exist: the factory cannot delete it');
        }
        unset(static::$_objects[$name]);
    }

    /**
     * Resets all the objects we know about and disabled MocksOnly
     *
     * @static
     * @return void
     */
    public static function reset()
    {
        static::$_objects = array();
        static::$_mocksOnly = FALSE;
    }


}