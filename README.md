Bairwell_DI
===========
This a module written in PHP 5.3.1+ (and, therefore, requires Namespaces support) which offers
dependency injection methods to PHP applications.

It allows you to setup mock objects to be injected and to turn off auto injection to allow
to unit testing.

Copyright
=========
This module was originally written by Richard Chiswell of Bairwell Ltd. Whilst Bairwell Ltd
holds the copyright on this work, we have licenced it under the MIT licence.

Bairwell Ltd: http://www.bairwell.com / Twitter: http://twitter.com/bairwell
Richard Chiswell: http://blog.rac.me.uk / Twitter: http://twitter.com/rchiswell

Licence
=======
This work is licensed under the MIT License

Copyright (c) 2011 Bairwell Ltd - http://www.bairwell.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Installation
============
The easiest way to install this module is to install it using the [PEAR Installer](http://pear.bairwell.com).
This installer is the PHP community's de-facto standard for installing PHP components.

    sudo pear channel-discover http://pear.bairwell.com
    sudo pear install --alldeps http://pear.bairwell.com/Bairwell_DI

Or to install the dist/Bairwell_DI...tgz file
    pear install dist/Bairwell_DI*.tgz

Usage
=====
    $object = \Bairwell\DI::getLibrary('\Mynamespace\MyClassPath\MyClass');
    $object->stuff();

    $valueobject= \Bairwell\DI::getValueObject('\Mynamespace\MyClassPath\MyValueObject');
    $object->stuff();

The best documentation for Bairwell_DI are the unit tests, which are shipped in the package.

You will find them installed into your PEAR repository, which on Linux systems is normally /usr/share/php/test.
Documentation in the source code is included in build/docblox .

To build
========
This is a PHIX ( http://phix-project.org ) compatible module and if you have PHIX installed you
can just do:
    phing test          <- to run the unit tests
    phing code-review   <- get code quality information
    phing phpdoc        <- get PhpDocumentor docs
    phing pear-package  <- to generate the package

This module has also been built with the Jenkins CI in mind ( http://www.jenkins-ci.org/ ) and
you can use the build.jenkins.xml file to run the tests and update Jenkins.

As A Dependency On Your Component
=================================
If you are creating a component that relies on Bairwell_DI, please make sure that you add Bairwell_DI to your component's package.xml file:

```xml
<dependencies>
  <required>
    <package>
      <name>Bairwell_DI</name>
      <channel>pear.bairwell.com</channel>
      <min>0.0.1</min>
      <max>0.999.999</max>
    </package>
  </required>
</dependencies>
```

Development Environment
=======================
If you want to patch or enhance this component, you will need to create a suitable development environment. The easiest way to do that is to install phix4componentdev:

    # phix4componentdev
    sudo apt-get install php5-xdebug
    sudo apt-get install php5-imagick
    sudo pear channel-discover pear.phix-project.org
    sudo pear -D auto_discover=1 install -Ba phix/phix4componentdev

You can then clone the git repository:

    # ComponentName
    git clone https://bitbucket.org/bairwell/di.git

Then, install a local copy of this component's dependencies to complete the development environment:

    # build vendor/ folder
    phing build-vendor

To make life easier for you, common tasks (such as running unit tests, generating code review analytics, and creating the PEAR package) have been automated using [phing](http://phing.info).  You'll find the automated steps inside the build.xml file that ships with the component.

Run the command 'phing' in the component's top-level folder to see the full list of available automated tasks.

ChangeLog
=========
0.3 : Add support for valueobjects/entities

0.2 : Add Exception if the class is unable to be loaded.
      Add custom exceptions
      Add requirement for full namespace and classname to be provided to prevent programmer error/clashes

0.1 : First release
